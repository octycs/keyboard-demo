from st3m.application import Application, ApplicationContext
import sys_buttons

# Simulator only
#from st3m.input import InputState
#from ctx import Context


LAYOUT_ABC_UPPER = (
    ("A", "B", "C"),
    ("D", "E", "F"),
    ("G", "H", "I"),
    ("J", "K", "L"),
    ("M", "N", "O"),
    ("P", "Q", "R"),
    ("S", "T", "U"),
    ("V", "W", "X"),
    ("Y", "Z", " "),
)
LAYOUT_ABC_LOWER = (
    ("a", "b", "c"),
    ("d", "e", "f"),
    ("g", "h", "i"),
    ("j", "k", "l"),
    ("m", "n", "o"),
    ("p", "q", "r"),
    ("s", "t", "u"),
    ("v", "w", "x"),
    ("y", "z", " "),
)
LAYOUT_ABC_SPECIAL1 = (
    ("[", "]", ","),
    ("1", "2", "3"),
    ("4", "5", "6"),
    ("7", "8", "9"),
    ("0", ".", "/"),
    ("(", ")", "?"),
    ("+", "-", "*"),
    ("_", ":", "!"),
    ("{", "}", "\""),
)
LAYOUT_ABC_SPECIAL2 = (
    ("<", ">", "\\"),
    ("&", "%", "$"),
    ("=", "~", ";"),
    ("#", " ", " "),#
    (" ", " ", " "),#
    (" ", " ", " "),#
    (" ", " ", " "),#
    (" ", " ", " "),#
    ("|", "^", "'"),
)

LAYOUTS = (
    (LAYOUT_ABC_LOWER, "abc"),
    (LAYOUT_ABC_UPPER, "ABC"),
    (LAYOUT_ABC_SPECIAL1, "123"),
    (LAYOUT_ABC_SPECIAL2, "<&>"),
)

POSITIONS = (
    #((   0, -109), (  0, -88), (  0, -67)),
    ((   0, -109), (  0, -93), (  0, -77), (0, -61)),
    ((  63,  -86), ( 51, -68), ( 39, -50)),
    (( 104,  -33), ( 85, -27), ( 67, -20)),
    (( 104,   34), ( 84,  28), ( 66,  22)),
    ((  63,   86), ( 51,  72), ( 39,  54)),
    ((   0,  109), (  0,  88), (  0,  67)),
    (( -63,   88), (-51,  71), (-37,  53)),
    ((-104,   35), (-88,  29), (-71,  23)),
    ((-104,  -33), (-85, -27), (-66, -23)),
    (( -65,  -89), (-53, -74), (-42, -57)),
)

LINE_SIZE = 14
UPDATE_TIMEOUT = 1000


class KeyboardDemo(Application):
    def __init__(self, app_ctx: ApplicationContext) -> None:
        super().__init__(app_ctx)

        self.current_layout = 0
        self.current_petal = None
        self.current_pos = None
        self.top_pressed = False
        self.any_pressed = False
        self.last_update = 0
        self.button_pressed = False

        self.buffer = []
        self.cursor_pos = 0
        self.window_pos = 0

    def _get_current_char(self):
        if self.current_petal is not None and self.current_pos is not None:
            return LAYOUTS[self.current_layout][0][self.current_petal - 1][self.current_pos]
        return ""

    def _update_char(self):
        if self.cursor_pos < len(self.buffer):
            self.buffer[self.cursor_pos] = self._get_current_char()
        else:
            self.buffer.append(self._get_current_char())
            self.cursor_pos = len(self.buffer) - 1

        self.last_update = 0

    def _confirm_char(self):
        self.cursor_pos += 1
        self.current_petal = None
        if self.cursor_pos - self.window_pos > LINE_SIZE - 1:
            self.window_pos += 1
        if self.current_petal and self.current_layout == 1:
            self.current_layout = 0

    def _delete_char(self):
        if len(self.buffer) == 0 or self.cursor_pos == 0:
            return
        if self.current_petal:  # Using this we practically cancel our current char
            self._confirm_char()
        self.cursor_pos -= 1
        self.buffer.pop(min(self.cursor_pos, len(self.buffer) - 1))
        if self.cursor_pos < self.window_pos + LINE_SIZE//2 and self.window_pos > 0:
            self.window_pos -= 1

    def _move_left(self):
        if self.cursor_pos > 0:
            self.cursor_pos -= 1
        if self.cursor_pos < self.window_pos + LINE_SIZE//2 and self.window_pos > 0:
            self.window_pos -= 1
        self.current_petal = None

    def _move_right(self):
        if self.cursor_pos < len(self.buffer):
            self.cursor_pos += 1
        if self.cursor_pos - self.window_pos > LINE_SIZE - 1:
            self.window_pos += 1
        self.current_petal = None

    def draw(self, ctx: Context) -> None:
        ctx.rgb(0.2, 0.2, 0.2).rectangle(-120, -120, 240, 240).fill()
        ctx.rgb(0, 0, 0).rectangle(-120, -120, 240, 80).fill()
        ctx.rgb(0, 0, 0).rectangle(-120, -120,  65, 240).fill()
        ctx.rgb(0, 0, 0).rectangle(  55, -120,  65, 240).fill()
        ctx.rgb(0, 0, 0).rectangle(-120,   40, 240, 80).fill()

        ctx.text_align = ctx.CENTER
        ctx.text_baseline = ctx.MIDDLE
        ctx.font = "Arimo Regular"

        ctx.font_size = 18
        for i, petal in enumerate(POSITIONS):
            for j, pos in enumerate(petal):
                ctx.move_to(*pos)
                if i == 0:
                    if j == self.current_layout:
                        ctx.rgb(.6, .6, 1)
                    else:
                        ctx.rgb(.7, .7, .7)
                    ctx.text(LAYOUTS[j][1])
                elif self.current_petal and i == self.current_petal and j == self.current_pos:
                    ctx.rgb(.6, .6, 1).text(LAYOUTS[self.current_layout][0][i - 1][j])
                else:
                    ctx.rgb(1, 1, 1).text(LAYOUTS[self.current_layout][0][i - 1][j])

        ctx.font_size = 14
        ctx.move_to(0, -12)
        ctx.rgb(.6, .6, .6).text("left: delete")
        ctx.move_to(0, 3)
        ctx.rgb(.6, .6, .6).text("right: next/space")
        ctx.move_to(0, 18)
        ctx.rgb(.6, .6, .6).text("down: done")
        ctx.move_to(0, 33)
        ctx.rgb(.6, .6, .6).text("l/r + touch p0: move")

        ctx.text_align = ctx.LEFT
        ctx.text_baseline = ctx.TOP
        ctx.font = "Comic Mono"
        ctx.font_size = 16

        ctx.move_to(-55, -38)
        ctx.rgb(.8, .8, .8)\
           .text("".join(self.buffer[
               max(0, self.window_pos)
               :min(len(self.buffer), self.window_pos + LINE_SIZE)
            ])
        )

        if self.cursor_pos < len(self.buffer):
            ctx.rgb(.6, .6, 1)\
               .rectangle(-55 + int(7.85 * (self.cursor_pos - self.window_pos)), -39, 8, 16)\
               .fill()
            ctx.move_to(-55 + int(7.85 * (self.cursor_pos - self.window_pos)), -38)
            ctx.rgb(0, 0, 0).text(self.buffer[self.cursor_pos])
        else:
            ctx.rgb(.6, .6, 1)\
               .rectangle(-55 + int(7.85 * (self.cursor_pos - self.window_pos)), -28, 8, 3)\
               .fill()

    def think(self, ins: InputState, delta_ms: int) -> None:
        super().think(ins, delta_ms)

        if ins.buttons.app != sys_buttons.NOT_PRESSED and not self.button_pressed:
            if ins.buttons.app == sys_buttons.PRESSED_LEFT and ins.captouch.petals[0].pressed:
                self._move_left()
                self.current_layout = 0
            elif ins.buttons.app == sys_buttons.PRESSED_LEFT:
                self._delete_char()
            elif ins.buttons.app == sys_buttons.PRESSED_DOWN:
                pass
            elif ins.buttons.app == sys_buttons.PRESSED_RIGHT and ins.captouch.petals[0].pressed:
                self._move_right()
                self.current_layout = 0
            elif ins.buttons.app == sys_buttons.PRESSED_RIGHT:
                if self.current_petal:
                    self._confirm_char()
                elif self.cursor_pos >= len(self.buffer):  # Enter space
                    self.current_layout = 0
                    self.current_petal = 9
                    self.current_pos = 2
                    self._update_char()
                    self._confirm_char()
                else:
                    self._move_right()
        elif ins.captouch.petals[0].pressed and not self.top_pressed:
            self.current_layout = (self.current_layout + 1) % len(LAYOUTS)
            if self.current_petal:
                self._confirm_char()
        elif not ins.captouch.petals[0].pressed:
            _any_pressed = False
            for i in range(1, 10):
                petal = ins.captouch.petals[i]
                if petal.pressed and not self.any_pressed:  # New touchdown
                    if self.current_petal == i:
                        self.current_pos = (self.current_pos + 1) % 3
                        self._update_char()
                    else:
                        if self.current_petal:
                            self._confirm_char()
                        self.current_pos = 0
                        self.current_petal = i
                        self._update_char()

                _any_pressed = _any_pressed or petal.pressed
            self.any_pressed = _any_pressed

            self.last_update += delta_ms
            if self.current_petal and self.last_update > UPDATE_TIMEOUT:
                self._confirm_char()
                self.last_update = 0

        self.button_pressed = ins.buttons.app != sys_buttons.NOT_PRESSED
        self.top_pressed = ins.captouch.petals[0].pressed


if __name__ == "__main__":
    import st3m.run

    st3m.run.run_view(KeyboardDemo(ApplicationContext()))
